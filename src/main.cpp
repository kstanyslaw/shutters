#include "Settings.h"
#include "shutter.h"

Shutter s1(13, 15, 14, 4);
Settings settings;
byte* shutterState = s1.getState();
byte oldState = *shutterState;
bool open = false; //debug

enum {
  WIFI_DISCONNECTED,
  WIFI_CONNECTED
} mode;

void setup()
{
  // DEBUG
    settings.writeOne(0, 0);
    Serial.begin(74880);
    Serial.print("Shutter state: ");
    Serial.println(*shutterState);

    Serial.print("EEPROM: ");
    Serial.println(settings.readOne(0));
  
}

void loop()
{
  s1.tick(); // Listen for Shutter s1

  if (oldState != *shutterState) {
    Serial.print("Shutter state: ");
    Serial.println(*shutterState);
    oldState = *shutterState;
  }

  // BEGIN DEBUG
  if (analogRead(A0) > 500 && open == true) {
    open = false;
    Serial.print("Shutter CLOSE! ");
    s1.startAutoClose();
    Serial.println(analogRead(A0));
  }

  if (analogRead(A0) < 350 && open == false) {
    open = true;
    Serial.print("Shutter OPEN! ");
    s1.startAutoOpen();
    Serial.println(analogRead(A0));
  }
  // END DEBUG
  
  /* sensors listener */
  /* web server */

  switch (mode)
  {
  case WIFI_DISCONNECTED:
    /* starting wifi-AP */
    /* searching for previous wifi station */
    break;
  
    case WIFI_CONNECTED:
    /* starting wifi-mesh */
    /* listen to internet commands*/
    break;
    
    default:
    break;
  }
}
