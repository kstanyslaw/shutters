#ifndef SHUTTER_H
#define SHUTTER_H

#include "Arduino.h"
#include "GyverButton.h"

class Shutter {
  private:
    byte state;
    unsigned int cycle;
    enum Mode {
        AUTO_OPENING,
        AUTO_CLOSING,
        OPENING,
        CLOSING,
        STOPPED,
        ERROR
    } mode;

    enum Direction {
        UP,
        DOWN
    };

    // Relay pins
    byte relay[2];

    // Buttons
    GButton *button[2];

    // Timer
    uint32_t btn_timer;

    bool flag[2];

    byte autoOpenStep();

    byte autoCloseStep();
    

  public:
    Shutter(byte _relay_up, byte _relay_down, byte _pin_button_up, byte _pin_button_down);

    void tick();

    unsigned int setCycle(unsigned int new_cycle);

    byte* getState();

    void startAutoOpen();

    void startAutoClose();

    byte open(Mode _mode);

    byte close(Mode _mode);

    byte stop();
};

#endif