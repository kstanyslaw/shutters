#ifndef SETTINGS_H
#define SETTINGS_H

#include "EEPROM.h"
#include "Arduino.h"

class Settings {
    private:
        byte value;

    public:
        Settings() {
            EEPROM.begin(512);
            // readAll();
        };

        byte readOne(int address) {
            return EEPROM.read(address);
        };

        byte readAll() {
            for (size_t i = 0; i < 512; i++)
            {
                value = EEPROM.read(i);
            }
            return value;
        };

        bool writeOne(int address, byte _value) {
            EEPROM.write(address, _value);
            EEPROM.commit();
            return true;
        };
};

#endif