#include "shutter.h"

Shutter::Shutter(byte _relay_up, byte _relay_down, byte _pin_button_up, byte _pin_button_down) {
    // Set default variables
    state = 0;
    cycle = 20000;
    flag[AUTO_OPENING] = false;
    flag[AUTO_CLOSING] = false;
    btn_timer = millis();

    // Setup relays
    relay[UP] = _relay_up;
    relay[DOWN] = _relay_down;
    pinMode(relay[UP], OUTPUT);
    pinMode(relay[DOWN], OUTPUT);

    // Setup buttons
    button[UP] = new GButton(_pin_button_up);
    button[DOWN] = new GButton(_pin_button_down);

    button[UP]->setStepTimeout(cycle/255);
    button[DOWN]->setStepTimeout(cycle/255);

    // Setup default relay state & mode
    stop();
}

void Shutter::tick() {
    button[UP]->tick();
    button[DOWN]->tick();

    if (button[UP]->isClick()) {
        switch (mode) {
        case STOPPED:
            flag[AUTO_OPENING] = true;            
            break;
        default:
            stop();
            break;
        }
    };

    if (button[DOWN]->isClick()) {
        switch (mode) {
        case STOPPED:
            flag[AUTO_CLOSING] = true;
            break;
        default:
            stop();
            break;
        }
    };

    if (button[UP]->isStep()) {
        if (state == 0) {
            state = 0;
            stop();
        } else {
            open(OPENING);
            state--;
        }
    };

    if (button[DOWN]->isStep()) {
        if (state == 255) {
            state = 255;
            stop();
        } else {
            close(CLOSING);
            state++;
        }
    };

    if (button[UP]->isRelease()) {
        switch (mode) {
        case OPENING:
            stop();
            break;
        
        default:
            break;
        }
    };

    if (button[DOWN]->isRelease()) {
        switch (mode) {
        case CLOSING:
            stop();
            break;
        
        default:
            break;
        }
    };

    if (flag[AUTO_OPENING]) {
        if (state > 0) {
            open(AUTO_OPENING);
            autoOpenStep();
        } else stop();
    }

    if (flag[AUTO_CLOSING]) {
        if(state < 255) {
            close(AUTO_CLOSING);
            autoCloseStep();
        } else stop();
    }

    if(mode == ERROR) {
        // return "ERROR while opening shutters";
    }
}

unsigned int Shutter::setCycle(unsigned int new_cycle) {
    cycle = new_cycle;
    return cycle;
}

byte* Shutter::getState()  {
    return &state;
}

void Shutter::startAutoOpen() {
    stop();
    flag[AUTO_OPENING] = true;
};

void Shutter::startAutoClose() {
    stop();
    flag[AUTO_CLOSING] = true;
};

byte Shutter::autoOpenStep() {
    if (millis() - btn_timer >= cycle/255) {
        btn_timer = millis();
        state--;
    };
    return state;    
}

byte Shutter::autoCloseStep()  {
    if (millis() - btn_timer >= cycle/255) {
        btn_timer = millis();
        state++;
    };
    return state;
}

byte Shutter::open(Mode _mode) {
    if (state > 0) {
        mode = _mode;
        digitalWrite(relay[UP], HIGH);
        digitalWrite(relay[DOWN], LOW);
    };
    return state;
}

byte Shutter::close(Mode _mode) {
    if (state < 255) {
        mode = _mode;
        digitalWrite(relay[UP], LOW);
        digitalWrite(relay[DOWN], HIGH);
    };
    return state;
}

byte Shutter::stop() {
    mode = STOPPED;
    flag[AUTO_OPENING] = false;
    flag[AUTO_CLOSING] = false;
    digitalWrite(relay[UP], LOW);
    digitalWrite(relay[DOWN], LOW);
    return state;
}